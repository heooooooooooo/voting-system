'use strict';
const mongoose = require('mongoose');
const Auth = require('./../servers/Auth');
/**
 * @api {post} /admin/login 管理员登陆获取token
 * @apiName login
 * @apiGroup admin
 *
 * @apiParam {String} username 候选人的名字.
 * @apiParam {String} pwd 密码
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "Authorization": "Basic dXNlcm5hbWU6cGFzc3dvcmQ="
 *     }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "success": true,
    "data": {
        "username": "username",
        "email": "zheng@qq.com",
        "_id": "5c4200140260f783d57bdb17",
        "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lIiwiaWF0IjoxNTQ3ODMwOTc1fQ.T9OSsEuzHTi_8OZMrYbZdOkWLGkRyLeUXSud-1tAD_c"
    }
}
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4012,
      "msg": "Username or password does not exist"
  }
 *
 */
exports.login = async (ctx)=>{
  const {username, pwd} = ctx.request.body;
  const where = {
    username: username,
  };
  const userInfo = await app.proxys.admin.getOne(where);
  if (!userInfo) return ctx.code = 4012;
  const _id = userInfo._id.toString();
  // 生成密码
  const auth = new Auth(app.conf.salt);
  const _pwd = auth.createAuth(username, _id, pwd);
  // 验证密码
  if (_pwd !== userInfo.pwd) return ctx.code = 4012;
  // 生成token
  const accessToken = app.tokenHelper.createToken({username: userInfo.username});
  const user = {
    username: userInfo.username,
    email: userInfo.email,
    _id: _id,
    accessToken: accessToken,
  };
  const userKey = app.okeys.adminToken(accessToken);
  await app.redisClient.setex(userKey, app.conf.adminTokenExpire, user);
  return ctx._body = user;
};

/**
 * @api {post} /candidate/add 增加候选人
 * @apiName add
 * @apiGroup candidate
 *
 * @apiParam {String} username 候选人的名字.
 * @apiParam {String} cid 选举配置id.
 * @apiParam {String} accessToken accessToken
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{ success: true,
  data:
   {
     _id: '5c41ce459142591eec9dc1e2',
     username: '郑崇',,
     cid: '5c41cf276171242364f65773',
     created_time: '2019-01-18T13:01:57.264Z',
     update_time: '2019-01-18T13:01:57.264Z',
     vote_count: 0
    }
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4012,
      "msg": "Missing required parameters"
  }
 *
 */
exports.add = async (ctx)=>{
  const {cid, username} = ctx.request.body;
  if (!cid || !username) return ctx.code = 4011;
  const param = {
    cid: mongoose.Types.ObjectId(cid),
    username: username,
    vote_count: 0,
  };
  const key = app.okeys.candidateProson(cid);
  const result = await app.proxys.pcand.save(param, key);
  return ctx._body = result;
};

/**
 * @api {delete} /candidate/:id 删除候选人
 * @apiName del
 * @apiGroup candidate
 *
 * @apiParam {String} id 候选人的id.
 * @apiParam {String} accessToken accessToken
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "accessToken": "dXNlcm5hbWU6cGFzc3dvcmQ="
 *     }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{ success: true,
  data:
   {
     id: '5c41ce459142591eec9dc1e2'
    }
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4012,
      "msg": "Missing required parameters"
  }
 *
 */
exports.del = async (ctx)=>{
  const {id} = ctx.params;
  if (!id) return ctx.code = 4011;
  const userInfo = await app.proxys.pcand.getOneInfo(id);
  const key = app.okeys.candidateProson(userInfo.cid);
  await app.proxys.pcand.removeUserInfo(id, key);
  return ctx._body = userInfo;
};

/**
 * @api {post} /candidate/update 更新候选人
 * @apiName update
 * @apiGroup candidate
 *
 * @apiParam {String} _id 候选人的id.
 * @apiParam {String} username 候选人的名字.
 * @apiParam {String} cid 选举配置id.
 * @apiParam {String} accessToken accessToken.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{ success: true,
  data:
   {
     _id: '5c41ce459142591eec9dc1e2',
     username: '郑崇',,
     cid: '5c41cf276171242364f65773',
     created_time: '2019-01-18T13:01:57.264Z',
     update_time: '2019-01-18T13:01:57.264Z',
     vote_count: 0
    }
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4011,
      "msg": "Missing required parameters"
  }
 *
 */
exports.update = async (ctx)=>{
  const {_id, username, cid} = ctx.request.body;
  if (!_id) return ctx.code = 4011;
  const params = {};
  if (username) {
    params.username= username;
  }
  if (cid) {
    params.cid = cid;
  }
  if (Object.keys(params).length > 0) {
    const update = {
      $set: params,
    };
    const key = app.okeys.candidateProson(cid);
    const result = await app.proxys.pcand.updateUserInfo(_id, update, key);
    return ctx._body = result;
  } else {
    return ctx.code = 4011;
  }
};

/**
 * @api {get} /candidate/list/:id 候选人列表
 * @apiName list
 * @apiGroup candidate
 *
 * @apiParam {String} id 候选人的id.
 * @apiParam {String} accessToken accessToken
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "accessToken": "dXNlcm5hbWU6cGFzc3dvcmQ="
 *     }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "success": true,
    "data": [
        {
            "_id": "5c41eb7531ec3b26080c8569",
            "cid": "5c41cf276171242364f65773",
            "username": "郑崇",
            "vote_count": 0,
            "created_time": "2019-01-18T15:06:29.776Z",
            "update_time": "2019-01-18T15:06:29.776Z",
            "__v": 0
        },
        ...
    ]
}
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4011,
      "msg": "Missing required parameters"
  }
 *
 */
exports.list = async (ctx)=>{
  const {id} = ctx.params;
  if (!id) return ctx.code = 4011;
  const key = app.okeys.candidateProson(id);
  const userList = await app.proxys.pcand.getListForCid(id, key);
  return ctx._body = userList;
};

// 配置时间
/**
 * @api {post} /conf/settime 配置选举时间
 * @apiName settime
 * @apiGroup conf
 *
 * @apiParam {String} id 配置id.
 * @apiParam {Number} startTs 开始时间 秒级时间戳.
 * @apiParam {Number} endTs 结束时间 秒级时间戳.
 * @apiParam {String} accessToken accessToken
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{ success: true,
  data:
   {
     _id: '5c41ce459142591eec9dc1e2',
     name: '美国大选',
     desc: '2019年美国总统选举',
     startTs: '2019-01-18T12:41:36.000Z',
     endTs: '2019-01-28T12:41:36.000Z',
     created_time: '2019-01-18T13:01:57.264Z',
     update_time: '2019-01-18T13:01:57.264Z',
     __v: 0
    }
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4010,
      "msg": "Name required param"
  }
 *
 */
exports.settime = async (ctx)=>{
  const {id, startTs, endTs} = ctx.request.body;
  if (!id || !startTs || !endTs) return ctx.code = 4011;
  if (!(endTs > startTs)) return ctx.code = 4013;
  const where = {
    _id: mongoose.Types.ObjectId(id),
  };
  const update = {
    $set: {
      startTs: startTs * 1000,
      endTs: endTs*1000,
    },
  };
  const key = app.okeys.canddidateConf();
  await app.proxys.ccand.update(where, update, {}, key);
  return ctx._body = true;
};

// 增加选举配置
/**
 * @api {post} /conf/addcandidate 增加选举配置
 * @apiName addcandidate
 * @apiGroup conf
 *
 * @apiParam {String} name 选举的名字.
 * @apiParam {String} desc 描述.
 * @apiParam {String} startTs 开始时间（毫秒）.
 * @apiParam {String} endTs 结束时间（毫秒）.
 * @apiParam {String} accessToken accessToken
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{ success: true,
  data:
   {
     _id: '5c41ce459142591eec9dc1e2',
     name: '美国大选',
     desc: '2019年美国总统选举',
     startTs: '2019-01-18T12:41:36.000Z',
     endTs: '2019-01-28T12:41:36.000Z',
     created_time: '2019-01-18T13:01:57.264Z',
     update_time: '2019-01-18T13:01:57.264Z',
     __v: 0
    }
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4010,
      "msg": "Name required param"
  }
 *
 */
exports.addcandidate = async (ctx)=>{
  let {name, desc, startTs, endTs} = ctx.request.body;
  if (!name) return ctx.code = 4010;
  if (!app.tool.moment.isDate(new Date(startTs))) {
    startTs = app.tool.moment().startOf('day').valueOf();
  }
  if (!app.tool.moment.isDate(new Date(endTs))) {
    endTs = app.tool.moment().add(7, 'days').endOf('day').valueOf();
  }
  const params = {
    name: name,
    desc: desc || '',
    startTs: startTs,
    endTs: endTs,
  };
  const result = await app.proxys.ccand.save(params);
  return ctx._body = result;
};


/**
 * @api {post} /conf/result/:id 查看结果
 * @apiName result
 * @apiGroup conf
 *
 * @apiParam {String} id 选举项的id.
 * @apiParam {String} accessToken accessToken
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "accessToken": "dXNlcm5hbWU6cGFzc3dvcmQ="
 *     }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        "success": true,
        "data": [
            {
                "_id": "5c41eb7531ec3b26080c8569",
                "cid": "5c41cf276171242364f65773",
                "username": "zhengchongupdate22222",
                "vote_count": "2"
            },
            {
                "_id": "5c41fa73c7b8a428c4cc28d2",
                "cid": "5c41cf276171242364f65773",
                "username": "郑崇2",
                "vote_count": "1"
            }
        ]
    }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4010,
      "msg": "Name required param"
  }
 *
 */
exports.result = async (ctx) => {
  const {id} = ctx.params;
  if (!id) return ctx.code = 4011;
  const key = app.okeys.voteCountRecord(id);
  const result = await app.redisClient.zrange(key, 0, -1);
  const userListkey = app.okeys.candidateProson(id);
  const userList = await app.proxys.pcand.getListForCid(id, userListkey);
  const obj = {};
  result.forEach((item, i) => {
    if (i % 2 == 0) {
      obj[item] = result[i + 1];
    }
  });
  for (let i = 0; i < userList.length; i++) {
    const _id = userList[i]._id;
    if (obj[_id]) {
      userList[i].vote_count = Number(obj[_id]) || 0;
    }
  }
  return ctx._body = userList;
};
