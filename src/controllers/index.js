'use strict';
const error = require('./error');
const candidate = require('./candidate');
const visitor = require('./visitor');

module.exports = {
  error: error,
  candidate: candidate,
  visitor: visitor,
};
