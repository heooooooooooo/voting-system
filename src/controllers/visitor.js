'use strict';
const conf = require('./../../conf');
const tokenHelper = require('./../utils/tokenHelper')(conf.tokenSignature);
/**
 * @api {post} /visitor/check 检测邮箱是否可用
 * @apiName checkemail
 * @apiGroup visitor
 *
 * @apiParam {String} email 用户邮箱.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
  {
      "success": true,
      "data": {
          "email": true
      }
  }
 * @apiError UserNotFound The <code>id</code> of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
    {
        "success": false,
        "code":4003,
        "msg":"The email has been registered"
    }
 *
 */
exports.checkemail = async (ctx)=>{
  const {email}= ctx.request.body;
  if (!email) return ctx.code = 4002;
  const voterSignatureKey = app.okeys.voterSignatureKey(email);
  const redisRes = await app.redisClient.getValByKey(voterSignatureKey);
  if (redisRes && redisRes.status==1) {
    return ctx.code = 4003;
  }
  return ctx._body={
    email: true,
  };
};

/**
 * @api {post} /visitor/register 发送验证邮件
 * @apiName register
 * @apiGroup visitor
 *
 * @apiParam {String} email 用户邮箱.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
  {
      "success": true,
      "data": {
          "msg": "The email was sent successfully. Please login to verify the email.",
          "email": "zhengchong470@gmail.com"
      }
  }
 * @apiError  UserNotFound The <code>id</code> of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4003,
      "msg": "Do not send mail repeatedly"
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4006,
      "msg": "The mail format is wrong"
  }
 *
 */
exports.register = async (ctx)=>{
  const {email}= ctx.request.body;
  if (!email) return ctx.code = 4002;
  // 验证邮件格式
  if (!/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(email)) return ctx.code = 4006;
  // 检测邮箱是否已被注册
  const voterSignatureKey = app.okeys.voterSignatureKey(email);
  // 检测是否1min内多次请求
  const key = app.okeys.visitorEmailRegistered(email);
  const result = await Promise.all([
    app.redisClient.getValByKey(voterSignatureKey),
    app.redisClient.getValByKey(key),
  ]);
  const voter = result[0];
  const redisRes = result[1];
  if (voter && voter.status==1) { // 已经注册过
    return ctx.code = 4003;
  }
  if (redisRes===null) {
    // 发送邮件到mq
    try {
      await app.public('emailRegisterEmail', email);
      //  加锁 过期时间60s 60s内只能发一次邮件
      await app.redisClient.setex(key, 60, 1);
      return ctx._body={
        msg: 'The email was sent successfully. Please login to verify the email.',
        email: email,
      };
    } catch (error) {
      return ctx.code = 4004;
    }
  } else { // 重复发送验证码
    return ctx.code = 4005;
  }
};

/**
 * @api {get} /visitor/verification/:token 验证邮件连接
 * @apiName verification
 * @apiGroup visitor
 *
 * @apiParam {String} token 用户token.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 {
    "success": false,
    "code": 4003,
    "msg": "The email has been registered"
  }
 *
 */
exports.verification = async (ctx)=>{
  const accessToken = ctx.params.token;
  if (!accessToken) return ctx.code = 4007;
  // 验证 注册
  const accessTokenKey = app.okeys.voterAccessToken(accessToken);
  const accessTokenInfo = await app.redisClient.getValByKey(accessTokenKey);
  if (!accessTokenInfo) return ctx.code = 4008;
  if (accessTokenInfo.status == 1) return ctx.code = 4003;
  // 更新redis信息
  accessTokenInfo.status = 1;
  const voterSignatureKey = app.okeys.voterSignatureKey(accessTokenInfo.email);
  await Promise.all([
    app.redisClient.setDataByKey(voterSignatureKey, accessTokenInfo),
    app.redisClient.setex(accessTokenKey, app.conf.voterTokenExpire, accessTokenInfo),
  ]);
  // 推送到mq落地数据库
  await app.public('emailRegisterToDb', accessTokenInfo.email);
  if (process.env.NODE_ENV == 'test') {
    return ctx._body = {
      accessToken,
    };
  } else {
    // 重定向到投票的首页 并将token带到url 供前端取到
    const url = `${app.conf.redirectUrl}?token=${accessToken}`;
    return ctx.response.redirect(url);
  }
};

/**
 * @api {post} /visitor/getToken 重新获取token
 * @apiName getToken
 * @apiGroup visitor
 *
 * @apiParam {String} email 用户邮箱.
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
  {
      "success": true,
      "data": {
          "accessToken": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InpoZW5nY2hvbmc0NzBAZ21haWwuY29tIiwiaWF0IjoxNTQ3NzQxODMzfQ.WdgIGvE-O7yUFac-VMLk5Hdtd0QDINcz5lN7ePH0MO8"
      }
  }
 * @apiError  UserNotFound The <code>id</code> of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4002,
      "msg": "Email required param"
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4009,
      "msg": "The email has been not registered"
  }
 *
 */
exports.getToken = async (ctx) => {
  const {email} = ctx.request.body;
  if (!email) return ctx.code = 4002;
  const voterInfo = await app.proxys.voter.getInfoByEmail(email);
  if (!voterInfo || voterInfo.status == 0) return ctx.code = 4009;
  const accessToken = tokenHelper.createToken({email: email});
  const voterAccessToken = app.okeys.voterAccessToken(accessToken);
  await app.redisClient.setex(voterAccessToken, app.conf.voterTokenExpire, voterInfo);
  return ctx._body = {
    accessToken,
  };
};

// 获取可投票的配置
/**
 * @api {get} /visitor/getconf 获取可投票的配置
 * @apiName getconf
 * @apiGroup visitor
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "success": true,
    "data": [
        {
            "_id": "5c41cf276171242364f65773",
            "name": "美国大选1",
            "desc": "2019年美国总统选举",
            "startTs": "2019-01-18T12:41:36.000Z",
            "endTs": "2019-01-28T12:41:36.000Z",
            "created_time": "2019-01-18T13:05:43.526Z",
            "update_time": "2019-01-18T13:05:43.526Z",
            "__v": 0
        },
        ...
    ]
}
 */
exports.getconf = async (ctx)=>{
  const key = app.okeys.canddidateConf();
  let result = await app.proxys.ccand.getListForAll(key);
  result = result.filter((item) => {
    return app.tool.moment(item.startTs).unix() < app.tool.moment().unix()
    && app.tool.moment(item.endTs).unix() > app.tool.moment().unix();
  });
  return ctx._body = result;
};

/**
 * @api {get} /visitor/list/:id 获取获选人及可投票数量
 * @apiName list
 * @apiGroup visitor
 *
 * @apiParam {String} id 选举项的id.
 * @apiHeaderExample {json} Header-Example:
 *     {
 *       "accessToken": "dXNlcm5hbWU6cGFzc3dvcmQ="
 *     }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
{
    "success": true,
    "data": {
        "list": [ // 列表
            {
                "_id": "5c41eb7531ec3b26080c8569",
                "cid": "5c41cf276171242364f65773",
                "username": "郑崇1",
                "vote_count": 0
            },
            ...
        ],
        "count": 2 // 可投票的数据
    }
}
 * @apiError  UserNotFound The <code>id</code> of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4002,
      "msg": "Email required param"
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4011,
      "msg": 'Missing required parameters'
  }
 *
 */
exports.list = async (ctx)=>{
  const {id} = ctx.params;
  if (!id) return ctx.code = 4011;
  const key = app.okeys.candidateProson(id);
  const countKey = app.okeys.candidateProson('count');
  const results= await Promise.all([
    app.proxys.pcand.getListForCid(id, key),
    app.proxys.pcand.getListCountByCid(id, countKey),
  ]);
  const result = results[0];
  const count = results[1];
  return ctx._body = {
    list: result || [],
    count: count,
  };
};

/**
 * @api {post} /visitor/submit 提交投票
 * @apiName submit
 * @apiGroup visitor
 *
 * @apiParam {String} id 候选人的id 多个用,隔开.
 * @apiParam {String} cid 选举项的id.
 * @apiParam {String} accessToken accessToken
 * @apiParamExample {json} Request-Example:
  {
    "cid":"5c41cf276171242364f65773",
    "id":"5c41eb7531ec3b26080c8569,5c41fa73c7b8a428c4cc28d2,5c41fa73c7b8a428c4cc28d2"
  }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        "success": true,
        "data": [
            "5c41eb7531ec3b26080c8569",
            "5c41fa73c7b8a428c4cc28d2"
        ]
    }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
    {
        "success": false,
        "code": 4013, // 投票的人数超过可投的人数
        "msg": "The params format is incorrect."
    }
 *
* @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
    {
        "success": false,
        "code": 4014, // 重复投票
        "msg": 'Don\'t repeat voting'
    }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4011,// 参数不对
      "msg": 'Missing required parameters'
  }
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4009,// 账号未认证
      "msg": 'The email has been not registered'
  }
 *
 */
exports.submit = async (ctx)=>{
  const {cid, id} = ctx.request.body;
  const userId = ctx.request.body.tokenInfo._id;
  if (ctx.request.body.tokenInfo.status == 0) return ctx.code = 4009;
  if (!cid || !id || !userId) return ctx.code = 4011;
  const ids = id.split(',');
  const countKey = app.okeys.candidateProson('count');
  const isVoteKey = app.okeys.voteKey(cid, userId);
  const isVote = await app.redisClient.getValByKey(isVoteKey);
  if (isVote) return ctx.code = 4014;
  const result = await app.proxys.pcand.getListCountByCid(cid, countKey);
  if (ids.length > result) {
    return ctx.code = 4013;
  }
  // 乐观锁 防止同个用户大量的请求进来利用处理请求的时间差间隙造成重复投票
  const voteLockKey = app.okeys.voteLockKey(cid, userId);
  const voteLockKeyResult = await app.redisClient.lockKey(voteLockKey, 60);
  if (voteLockKeyResult == 1) {
    try {
      // 最后记录投票数量记录
      const command = [];
      const keys = app.okeys.voteCountRecord(cid);
      for (let i = 0; i < ids.length; i++) {
        command.push(['ZINCRBY', keys, 1, ids[i]]);
      }
      await app.redisClient.execMulit(command);
      // 推送到mq
      await app.public('voteCountRecordToDb', `${cid}:${userId}:${id}`);
      // 记录投票者的投票
      await app.redisClient.setnx(isVoteKey, id);
    } catch (error) { }
    // 释放锁
    await app.redisClient.unlockKey(voteLockKey);
  };
  return ctx._body = ids;
};

/**
 * @api {post} /visitor/checkvote 检测该用户是否还能投票
 * @apiName checkvote
 * @apiGroup visitor
 *
 * @apiParam {String} cid 选举项的id.
 * @apiParam {String} accessToken accessToken
 * @apiParamExample {json} Request-Example:
  {
    "cid":"5c41cf276171242364f65773",
  }
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
    {
        "success": true,
        "data":true/false
    }
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
  {
      "success": false,
      "code": 4011,// 参数不对
      "msg": 'Missing required parameters'
  }
 */
exports.checkvote = async (ctx)=>{
  const {cid} = ctx.request.body;
  const userId = ctx.request.body.tokenInfo._id;
  if (!cid || !userId) return ctx.code = 4011;
  const isVoteKey = app.okeys.voteKey(cid, userId);
  const isVote = await app.redisClient.getValByKey(isVoteKey);
  if (isVote) return ctx._body = false;
  else return ctx._body = true;
};
