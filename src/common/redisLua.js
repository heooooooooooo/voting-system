/**
 *
 * lua脚本集合
 *
 * 用于一些对redis原有数据有执行依赖的事务
 */

// redis连接，本身不创建，由外部传入。
let redisClient;

const instance = {
  script: {
  },
};

// 用于记录已在redis缓存过的脚本sha码
const bufferScript = {};
/**
 * 抢红包动作的脚本定义（keysLength值为KEY的个数）
 *
 * KEYS[1]      锁key
 * ARGV[1]      过期时间
 *
 * @return 0已投过  1 未投过
 */
instance.script.isVote = {
  code: `
    if(redis.call('setnx', KEYS[1], 1)==1)
    then
      if(redis.call('expire', KEYS[1], ARGV[1]))
      then
        return 1
      else 
        redis.call('del', KEYS[1])
        return 0
      end
    else
      return 0
    end
  `,
  keysLength: 1,
};


/**
 *
 * lua执行器 自动判断是否已经缓存过  从而决定是向redis传递脚本还是sha
 *
 * @param name    本脚本所支持的指令  位于 instance.script 下
 * @param ...param  该指令所期待的参数, 按照KEYS到ARGV的顺序罗列
 */
instance.run = function(name, ...param) {
  return new Promise((resolve, reject) => {
    if (!redisClient) {
      reject('redisClient is no ready');
    } else if (!instance.script[name]) {
      reject('this command is not supported');
    } else {
      if (bufferScript[name]) {
        redisClient.evalsha(bufferScript[name], instance.script[name].keysLength, ...param, (err, result) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
      } else {
        redisClient.script('load', instance.script[name].code, (err, sha) => {
          if (err) {
            reject(err);
          } else {
            bufferScript[name] = sha;
            redisClient.evalsha(sha, instance.script[name].keysLength, ...param, (err, result) => {
              if (err) {
                reject(err);
              } else {
                resolve(result);
              }
            });
          }
        });
      }
    }
  });
};


module.exports = function(client) {
  if (!client) {
    return;
  }
  redisClient = client;
  return instance;
};
