'use strict';
const RedisHelper = require('./../utils/redisHelper');

module.exports = class CacheServer extends RedisHelper {
  /**
   *
   * @param {*} param0
   */
  constructor({model, options}) {
    super(options);
    this.name = model;
    this.on('update', this.delKeyCache);
  }

  /**
   *
   * @param {*} key
   */
  async delKeyCache(key, ...arg) {
    const field = arg[0];
    if (field && key) {
      this.delValByField(key, field);
    } else {
      key && this.delValByKey(key);
    }
  }

  /**
   *
   * @param {*} val
   * @param {*} field
   * @param  {...any} arg
   */
  async setCache(val, field, ...arg) {
    const key = arg[0] ? `${this.name}:${arg[0]}` : this.name;
    if (field) {
      this.setDataByfield(key, field, val);
    } else {
      this.setDataByKey(key);
    }
  }
};
