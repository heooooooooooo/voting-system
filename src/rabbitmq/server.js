'use strict';
const conf = require('./../../conf');
const sendemail = require('./../servers/Email')(conf.email, conf.url);
const tokenHelper = require('./../utils/tokenHelper')(conf.tokenSignature);
const mongoose = require('mongoose');
/**
 * 接收mq队列 发送邮件
 */
exports.emailRegisterEmail = async (msg)=>{
  try {
    const email = msg.content.toString();
    const accessToken = tokenHelper.createToken({email: email});
    const params = {
      email: email,
      status: 0,
    };
    const voter = await app.proxys.voter.findOneAndUpdate(params, params,
        {upsert: true, new: true, rawResult: true, setDefaultsOnInsert: true});
    if (voter.ok == 1) {
      params._id = voter.value._id;
      params.saveTime = app.tool.moment().unix();
      const voterSignatureKey = app.okeys.voterSignatureKey(email);
      const voterAccessToken = app.okeys.voterAccessToken(accessToken);
      await Promise.all([
        app.redisClient.setnx(voterSignatureKey, params),
        app.redisClient.setex(voterAccessToken, app.conf.voterTokenExpire, params),
      ]);
      await sendemail.sendEmailToMQ(email, accessToken);
    }
  } catch (error) {
    console.log(error);
  }
};

/**
 * 接收注册信息落地数据库
 */
exports.emailRegisterToDb = async (msg)=>{
  try {
    const email = msg.content.toString();
    const where = {
      email: email,
    };
    const update = {
      $set: {
        status: 1,
      },
    };
    await app.proxys.voter.update(where, update);
  } catch (error) {
    console.log(error);
  }
};

/**
 * 投票数量落地数据库
 */
exports.voteCountRecordToDb = async (msg) => {
  const message = msg.content.toString();
  if (!message) return;
  const messageBody = message.split(':');
  if (messageBody.length !== 3) {
    return;
  }
  const userId = messageBody[1];
  let ids = messageBody[2];
  ids = ids.split(',');
  const dbComand = [];
  const update_p = {
    $inc: {
      vote_count: 1,
    },
  };
  const where_v = {
    'status': 1,
    '_id': mongoose.Types.ObjectId(userId),
  };
  const update_v = {
    $addToSet: {
      candidateId: {
        $each: ids,
      },
    },
  };
  const result = await app.proxys.voter.update(where_v, update_v);
  if (result.od == 1 && result.nModified == 1) {
    {for (let i = 0; i < ids.length; i++) {
      dbComand.push({
        updateOne: {
          filter: {_id: mongoose.Types.ObjectId(ids[i])},
          update: update_p,
        },
      });
    }}
    await app.proxys.pcand.bulkWrite(dbComand);
  }
};
