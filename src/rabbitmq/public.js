'use strict';
module.exports = async (ch)=>{
  console.log('public');
  return async (queuename, message)=>{
    ch.assertQueue(queuename, {durable: true});
    return ch.sendToQueue(queuename, new Buffer(message));
  };
};
