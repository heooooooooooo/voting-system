'use strict';
const server = require('./server');
module.exports = async function(ch) {
  for (const key in server) {
    if (Object.prototype.hasOwnProperty.call(server, key)) {
      ch.assertQueue(key, {durable: true});
      ch.consume(key, async function(msg) {
        await server[key](msg);
        ch.ack(msg);
      }, {noAck: false});
    }
  }
};
