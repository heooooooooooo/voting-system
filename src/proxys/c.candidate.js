'use strict';
const Base = require('./Base');
module.exports = class Ccand extends Base {
  /**
   *
   * @param {*} model
   */
  constructor(model) {
    super(model);
  }

  /**
   *
   * @param {*} key
   */
  async getListForAll(key) {
    let result= [];
    if (key) {
      result = await this.getValByKey(key);
      if (!result) {
        result = await this.getList();
        await this.setDataByKey(key, result);
      }
    } else {
      result = await this.getList();
    }
    return result;
  }
};
