'use strict';
const Base = require('./Base');
module.exports = class Pcand extends Base {
  /**
   *
   * @param {*} model
   */
  constructor(model) {
    super(model);
  }

  /**
   *
   * @param {*} id
   */
  async getOneInfo(id) {
    return this.model.findById(id);
  }

  /**
   *
   * @param {*} id
   * @param  {...any} arg
   */
  async removeUserInfo(id, ...arg) {
    await this.model.findByIdAndRemove(id);
    this.emit('update');
    this.emit('update', arg[0]);
  }

  /**
   *
   * @param {*} id
   * @param {*} key
   */
  async getUserListById(id, key) {
    let result = key?await this.getDataByfield(key, id):null;
    if (!result) {
      const where = {
        cid: id,
      };
      result = await this.model.find(where, {_id: 1, cid: 1, username: 1, vote_count: 1});
      (result && key) && await this.setDataByfield(key, id, result);
    }
    return result;
  }

  /**
   *
   * @param {*} id
   * @param {*} update
   * @param {*} key
   */
  async updateUserInfo(id, update, key) {
    const result = await this.model.findByIdAndUpdate(id, update, {new: true});
    await this.delValByKey(key);
    return result;
  }

  /**
   * 根据cid获取该选举的获选人
   * @param {*} cid
   * @param {*} key
   */
  async getListForCid(cid, key) {
    let result = await this.getDataByfield(key, cid);
    if (!result) {
      try {
        result = await this.getUserListById(cid);
        await this.setDataByfield(key, cid, result);
      } catch (error) {
        console.log(error);
      }
    }
    return result;
  }

  /**
   *
   * @param {*} cid
   * @param {*} countKey
   */
  async getListCountByCid(cid, countKey) {
    let count = await this.getDataByfield(countKey, cid);
    if (count == null || count == undefined) {
      count = 0;
      const result = await this.getUserListById(cid);
      if (Array.isArray(result)) {
        const len = result.length;
        if (len <= 2) {
          count = len;
        } else if (len < 10) {
          count = 2;
        } else {
          count = Math.ceil(len * 0.2);
        }
        await this.setDataByfield(countKey, cid, count);
      }
    }
    return count;
  }
};
