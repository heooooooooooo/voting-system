'use strict';
const models = require('./../models');
const CacheServer = require('./../common/CacheServer');
const redisOption = require('./../../conf').redis;
module.exports = class Base extends CacheServer {
  /**
   *
   * @param {String} model
   */
  constructor(model) {
    super({model: model, options: redisOption});
    this.model = models[model];
  }

  /**
   *
   * @param {Object} where
   * @param {Object} fileds
   * @param {Object} options
   */
  async getList(where = {}, fileds = {}, options = {}, ...args) {
    const result = await this.model.find(where, fileds, options);
    const key = args[0];
    if (key) {
      this.emit('save', key, result);
    }
    return result;
  }
  /**
   *
   * @param {Object} where
   * @param {Object} fileds
   */
  getOne(where={}, fileds={}) {
    return this.model.findOne(where, fileds);
  }

  /**
   *
   * @param {Object} where
   */
  count(where) {
    return this.model.count(where);
  }

  /**
   *  最后个参数为redis缓存的field 表示是否要删除该缓存
   * @param {*} params
   * @param  {...any} args
   */
  async save(params, ...args) {
    const Model = this.model;
    const _params = new Model(params);
    const result = await _params.save();
    const fileds = args[0];
    if (fileds) {
      this.emit('update', fileds);
    }
    return result;
  }

  /**
   *
   * @param {*} where
   * @param {*} update
   * @param {*} options
   */
  async update(where, update, options = {}, ...args) {
    if (!where) return Promise.reject('missing where');
    const result = await this.model.update(where, update, options);
    const fileds = args[0];
    if (fileds) {
      this.emit('update', fileds);
    }
    return result;
  }

  /**
   *
   * @param {*} where
   * @param {*} update
   * @param {*} options
   */
  async findOneAndUpdate(where, update, options = {}, ...args) {
    if (!where) return Promise.reject('missing where');
    const result = await this.model.findOneAndUpdate(where, update, options);
    const fileds = args[0];
    if (fileds) {
      this.emit('update', fileds);
    }
    return result;
  }
  /**
   * 批量执行
   * @param {*} command
   */
  bulkWrite(command) {
    return this.model.bulkWrite(command);
  }
};
