'use strict';
const Base = require('./Base');
const okeys= require('./../configs/keys');
module.exports = class Voter extends Base {
  /**
   *
   * @param {*} model
   */
  constructor(model) {
    super(model);
  }

  /**
   *
   * @param {*} email
   */
  async getInfoByEmail(email) {
    const key = okeys.voterSignatureKey(email);
    let result = await this.getValByKey(key);
    if (!result) {
      result = await this.getOne({email: email}, {_id: 1, email: 1, status: 1});
      await this.setDataByKey(key, result);
    }
    return result;
  }
};
