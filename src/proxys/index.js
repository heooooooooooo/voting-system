'use strict';
const Admin = require('./admin');
const Voter = require('./voter');
const Ccand = require('./c.candidate');
const Pcand = require('./p.candidater');

module.exports = {
  admin: new Admin('admin'),
  voter: new Voter('voter'),
  ccand: new Ccand('cCandidate'),
  pcand: new Pcand('pCandidater'),
};
