'use strict';
const error = require('./../configs/error');
const auth = require('basic-auth');
exports.response = async (ctx, next)=>{
  // request start time
  const start = new Date();
  try {
    await next();
    // log response
    app.logUtil.logResponse(ctx, new Date() - start);
    // Package Response object
    if (ctx._body!==undefined) {
      ctx.body = {
        success: true,
        data: ctx._body,
      };
    } else if (ctx.code) {
      ctx.body = {
        success: false,
        code: ctx.code||4001,
        msg: error[ctx.code]||error[4001],
      };
    } else {
      ctx.body = {
        success: false,
        code: 4000,
        msg: error[4000],
      };
    }
  } catch (err) {
    // throw error and log error message
    app.logUtil.logError(ctx, err, new Date() - start);
    ctx.body = {
      success: false,
      code: 4000,
      msg: error[4001],
    };
  }
};

/**
 * 检测token中间件
 * 返回一个方法
 */
exports.checkToken =(type) => {
  return async (ctx, next) => {
    let accessToken;
    if (ctx.request.body.accessToken) {
      accessToken = ctx.request.body.accessToken;
    } else if (ctx.params.token) {
      accessToken = ctx.params.token;
    } else if (ctx.query.token) {
      accessToken = ctx.query.token;
    } else {
      accessToken = ctx.header['accesstoken'];
    }

    if (!accessToken) {
      return ctx.code = 4007;
    } else {
      const result = type == 'admin' ? await exports._checkAdminToken(accessToken) : await exports._checkVoterToken(accessToken);
      if (!result.tokenInfo) return ctx.code = 4007;

      if (Object.keys(ctx.request.body).length > 0) ctx.request.body.tokenInfo = result.tokenInfo;
      else if (Object.keys(ctx.params).length>0) ctx.params.tokenInfo = result.tokenInfo;
      else if (Object.keys(ctx.params).length > 0) ctx.params.tokenInfo = result.tokenInfo;
      else ctx.tokenInfo = tokenInfo;

      // 更新 token过期时间
      await app.redisClient.setex(result.tokenKey, app.conf.voterTokenExpire, result.tokenInfo);
    }
    await next();
  };
};

/**
 * 获取投票者信息
 */
exports._checkVoterToken = async (accessToken)=>{
  const tokenKey = app.okeys.voterAccessToken(accessToken);
  const tokenInfo = await app.redisClient.getValByKey(tokenKey);
  return {tokenInfo, tokenKey};
};

/**
 * 获取管理员信息
 */
exports._checkAdminToken = async (accessToken) => {
  const tokenKey = app.okeys.adminToken(accessToken);
  const tokenInfo = await app.redisClient.getValByKey(tokenKey);
  return {tokenInfo, tokenKey}; ;
};

/**
 * 账号密码获取中间件
 */
exports.BaseAuth = () => {
  return async (ctx, next) => {
    const user = auth(ctx.req);
    if (user) {
      ctx.request.body = {
        username: user.name,
        pwd: user.pass,
      };
      await next();
    } else {
      ctx.status = 401;
      return ctx.code = 4011;
    }
  };
};


