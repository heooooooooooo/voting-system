const cryptoHelper = require('./../utils/cryptoHelper');
module.exports = class Auth {
  /**
   *
   * @param {*} salt
   */
  constructor(salt) {
    this.salt = salt;
  }

  /**
   *
   * @param {*} username
   * @param {*} _id
   * @param {*} pwd
   */
  createAuth(username, _id, pwd) {
    const salt = cryptoHelper.MD5(_id + this.salt + username );
    const _pwd = cryptoHelper.SHA256(salt + pwd);
    return _pwd;
  }
};
