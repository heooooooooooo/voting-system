const nodemailer = require('nodemailer');
/**
 *发送email服务
 */
class Email {
  /**
   *
   * @param {*} options
   * @param {*} url
   */
  constructor(options, url) {
    this.options = options;
    this.url = url;
    this.mailTransport = nodemailer.createTransport(this.options);
  }

  /**
   *
   * @param {*} email
   * @param {*} accessToken
   */
  async sendEmailToMQ(email, accessToken) {
    const options = {
      from: this.options.auth.user,
      to: email,
      subject: '邮箱验证',
      text: '邮箱验证',
      html: `<a href="${this.url}?token=${accessToken}">点击链接完成</a>`,
    };
    return new Promise((resolve, reject)=>{
      this.mailTransport.sendMail(options, function(err, msg) {
        if (err) reject(err);
        else resolve(msg);
      });
    });
  }
}

module.exports = function(options, url) {
  return new Email(options, url);
};
