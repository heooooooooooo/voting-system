'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const admin = require('./admin');
const cCandidate = require('./c.candidate');
const voter = require('./voter');
const pCandidater = require('./p.candidater');

const adminSchema = new Schema(admin, {collection: 'admin'});
const cCandidateSchema = new Schema(cCandidate, {collection: 'c.candidate'});
const voterSchema = new Schema(voter, {collection: 'voter'});
const pCandidaterSchema = new Schema(pCandidater, {collection: 'p.candidater'});

module.exports = {
  admin: mongoose.model('admin', adminSchema),
  cCandidate: mongoose.model('cCandidate', cCandidateSchema),
  voter: mongoose.model('voter', voterSchema),
  pCandidater: mongoose.model('pCandidater', pCandidaterSchema),
};
