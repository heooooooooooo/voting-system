'use strict';
const mongoose =require('mongoose');
module.exports ={
  username: String,
  vote_count: Number, // 获得投票数量
  cid: mongoose.Schema.Types.ObjectId, // 关联 c.candidate表的_id
  created_time: {type: Date, default: Date.now},
  update_time: {type: Date, default: Date.now},
};
