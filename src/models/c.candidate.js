'use strict';
module.exports ={
  name: String,
  desc: String,
  startTs: Date,
  endTs: Date,
  created_time: {type: Date, default: Date.now},
  update_time: {type: Date, default: Date.now},
};
