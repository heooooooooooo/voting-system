'use strict';
module.exports ={
  email: {type: String, index: true, unique: true},
  registeredIp: String, // 注册ip
  status: Number, // 默认0  0.没有验证 1.已验证
  candidateId: {
    type: Array,
    value: [],
  },
  created_time: {type: Date, default: Date.now},
  update_time: {type: Date, default: Date.now},
};
