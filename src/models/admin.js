'use strict';
const mongoose =require('mongoose');
module.exports ={
  _id: mongoose.Schema.Types.ObjectId,
  username: String,
  email: String,
  pwd: String,
  auth: {
    default: 100,
    type: Number,
  }, // 权限
  created_time: {type: Date, default: Date.now},
  update_time: {type: Date, default: Date.now},
};
