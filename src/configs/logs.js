const config = {
  appenders: {
    out: {type: 'console'},
    result: {'type': 'dateFile', 'filename': 'logs/result', 'pattern': '-dd.log', 'alwaysIncludePattern': true},
    errors: {'type': 'dateFile', 'filename': 'logs/errors', 'pattern': '-dd.log', 'alwaysIncludePattern': true},
    default: {'type': 'dateFile', 'filename': 'logs/default', 'pattern': '-dd.log', 'alwaysIncludePattern': true},
  },
  categories: {
    default: {appenders: ['out', 'default'], level: 'info'},
    result: {appenders: ['result'], level: 'info'},
    error: {appenders: ['errors'], level: 'info'},
  },
};
module.exports = config;
