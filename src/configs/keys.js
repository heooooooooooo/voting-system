/**
 * redis 统一key管理
 */
module.exports = class Keys {
  /**
   *
   */
  constructor() {

  }

  /**
   * @param {String} type
   * @param {String} name
   */
  static _token(type, name) {
    return `${type}AccessToken:${name}`;
  }

  /**
   *
   * @param {String} email
   */
  static adminToken(email) {
    return Keys._token('admin', email);
  }
  /**
   *
   * @param {String} email
   */
  static visitorEmailRegistered(email) {
    return `email-registered:${email}`;
  }

  /**
   *
   * @param {*} email
   */
  static voterSignatureKey(email) {
    return `voterSignatureKey:${email}`;
  }

  /**
   *
   * @param {*} email
   */
  static voterAccessToken(email) {
    return Keys._token('voter', email); ;
  }

  /**
  * 缓存选举的候选人
  * @param {*} cid
  */
  static candidateProson(type) {
    return type ? `candidateProson:${type}` :'candidateProson';
  }


  /**
   * 缓存选举的配置
   */
  static canddidateConf() {
    return `canddidateConf`;
  }

  /**
   * 加锁 防止重复投票
   * @param {*} cid
   * @param {*} userId
   */
  static voteLockKey(cid, userId) {
    return `voteLockKey:${cid}:${userId}`;
  }

  /**
   * 加锁 防止重复投票
   * @param {*} cid
   * @param {*} userId
   */
  static voteKey(cid, userId) {
    return `voteKey:${cid}:${userId}`;
  }

  /**
   * 记录投票数
   */
  static voteCountRecord(cid) {
    return `voteCountRecord:${cid}`;
  }
};
