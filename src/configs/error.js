/**
 * 错误码对应的详细信息
 */
module.exports= {
  '4000': 'Undefined uri',
  '4001': 'Undefined error',
  '4002': 'Email required param',
  '4003': 'The email has been registered',
  '4004': 'Send email failure Please one more time',
  '4005': 'Do not send mail repeatedly',
  '4006': 'The mail format is wrong',
  '4007': 'AccessToken is required',
  '4008': 'Invaild accessToken',
  '4009': 'The email has been not registered',
  '4010': 'Name required param',
  '4011': 'Missing required parameters',
  '4012': 'Username or password does not exist',
  '4013': 'The params format is incorrect.',
  '4014': 'Don\'t repeat voting',
};
