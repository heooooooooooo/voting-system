'use strict';
const Router = require('koa-router');
const router = new Router();
const controllers = require('./../controllers');
const middleWare = require('./../middleware');

// 测试中间件路由
if (process.env.NODE_ENV == 'test') {
  router.post('/test/log', controllers.error.log);
  router.post('/test/err', controllers.error.err);
  router.post('/test/error', controllers.error.error);
}

// admin 模块路由
router.post('/candidate/login', middleWare.BaseAuth(), controllers.candidate.login);
router.post('/candidate/addcandidate', middleWare.checkToken('admin'), controllers.candidate.addcandidate);
router.post('/candidate/add', middleWare.checkToken('admin'), controllers.candidate.add);
router.delete('/candidate/:id', middleWare.checkToken('admin'), controllers.candidate.del);
router.post('/candidate/update', middleWare.checkToken('admin'), controllers.candidate.update);
router.get('/candidate/list/:id', middleWare.checkToken('admin'), controllers.candidate.list);
router.post('/conf/settime', middleWare.checkToken('admin'), controllers.candidate.settime);
router.get('/conf/result/:id', middleWare.checkToken('admin'), controllers.candidate.result);

// 访客 模块路由
router.post('/visitor/check', controllers.visitor.checkemail);
router.post('/visitor/register', controllers.visitor.register);
router.get('/visitor/verification/:token', controllers.visitor.verification);
router.get('/visitor/getconf', controllers.visitor.getconf);
router.get('/visitor/list/:id', controllers.visitor.list);
router.post('/visitor/submit', middleWare.checkToken('voter'), controllers.visitor.submit);
router.post('/visitor/checkvote', middleWare.checkToken('voter'), controllers.visitor.checkvote);
router.post('/visitor/getToken', controllers.visitor.getToken);

module.exports = router;
