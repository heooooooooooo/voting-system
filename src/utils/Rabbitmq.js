'use strict';
const amqp = require('amqplib');
const EventEmitter = require('events');
module.exports = class RabbitmqHelper extends EventEmitter {
  /**
   *
   * @param {*} option
   */
  constructor(option) {
    super();
    this.options = option;
    this.num = 0;
    this.isRestart = false;
    this.timer = null;
    this.on('close', () => {
      if (!(this.num > 10)) {
        this.restart();
      }
    });

    this.on('error', () => {
      if (!(this.num > 10)) {
        this.restart();
      }
    });
    this.connect();
  }

  /**
   *
   */
  async connect() {
    this.connection = amqp.connect(this.options);
  }

  /**
   *
   */
  async createChannel() {
    const _this = this;
    const ch = await new Promise((resolve, reject) => {
      _this.connection.then((conn) => {
        console.log('rabbitmq connecting!!!');
        conn.on('error', function(err) {
          console.log('Error from amqp: ', err);
        });

        conn.on('close', function(err) {
          console.log('close from amqp: ', err);
          _this.emit('close');
        });

        conn.on('blocked', function(err) {
          console.log('blocked from amqp: ', err);
        });

        conn.on('unblocked', function(err) {
          console.log('unblocked from amqp: ', err);
        });
        return conn.createChannel();
      }).then((_ch) => {
        resolve(_ch);
      }).catch((e) => {
        _this.emit('error');
        console.error(e);
        reject(e);
      });
    });
    return ch;
  }

  /**
   *重新mq连接
   */
  restart() {
    if (!this.timer) {
      this.timer = setTimeout(() => {
        this.connect();
        this.createChannel();
        this.timer = null;
        this.num++;
        this.emit('success');
      }, this.options.reconnectTime || 5000);
    }
  }
};
