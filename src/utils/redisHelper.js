'use strict';
const redis = require('redis');
const _ = require('lodash');
const Event = require('events');
const RedisLua = require('./../common/redisLua');
module.exports = class RedisHelper extends Event {
  /**
   * 对redis操作进行再次业务封装，将json对象转成string，或者将string转成json对象
   * @param {*} options
   */
  constructor(options) {
    super();
    this.url = options.host;
    this.option = options.option || {};
    this.client = redis.createClient(this.url, this.option);
  }

  /**
  * 将对象存进redis
  * @param {*} key
  * @param {*} value
  * @param {*} callback
  */
  async setDataByKey(key, value, callback) {
    let _val = value;
    if (!_.isBoolean(_val) || !_.isNumber(_val)) {
      _val = JSON.stringify(_val);
    }
    const result = await new Promise((resolve, reject) => {
      this.client.set(key, _val, function(err, result) {
        if (err) reject(err);
        else resolve(result);
      });
    });
    return _.isFunction(callback) ? callback(null, result) : result;
  }

  /**
  * 将对象存进redis
  * @param {*} key
  * @param {*} filed
  * @param {*} value
  * @param {*} callback
  */
  async setDataByfield(key, filed, value, callback) {
    let _val = value;
    if (!_.isBoolean(_val) || !_.isNumber(_val)) {
      _val = JSON.stringify(_val);
    }
    const result = await new Promise((resolve, reject) => {
      this.client.hset(key, filed, _val, function(err, result) {
        if (err) reject(err);
        else resolve(result);
      });
    });
    return _.isFunction(callback)?callback(null, result):result;
  }

  /**
  * 取出redis里面的数据转成对象
  * @param {*} key
  * @param {*} filed
  * @param {*} callback
  */
  async getDataByfield(key, filed, callback) {
    const result =await new Promise((resolve, reject) => {
      this.client.hget(key, filed, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
    let _result = result;
    try {
      _result = JSON.parse(_result);
    } catch (error) {
      _result = result;
    }
    return _.isFunction(callback) ? callback(null, _result) : _result;
  }

  /**
   * 创建唯一key
   * @param {*} key
   * @param {*} value
   */
  async setnx(key, value) {
    let val = value;
    if (_.isObject(val)) {
      val = JSON.stringify(val);
    }
    return new Promise((resolve, reject)=>{
      this.client.SETNX(key, val, function(err, res) {
        if (err) reject(err);
        else resolve(res);
      });
    });
  }

  /**
   *根据key获取
   * @param {*} key
   */
  async getValByKey(key, callback) {
    const result = await new Promise((resolve, reject)=>{
      this.client.get(key, function(err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
    let _result = result;
    try {
      _result = JSON.parse(_result);
    } catch (error) {
      console.log(error);
    }
    return _.isFunction(callback) ? callback(null, _result) : _result;
  }

  /**
   *创建key并加上过期时间
   * @param {*} key
   * @param {*} time 默认60s
   * @param {*} value
   */
  setex(key, time=60, value=1) {
    let val = value;
    if (_.isObject(val)) {
      val = JSON.stringify(val);
    }
    return new Promise((resolve, reject)=>{
      this.client.setex(key, time, val, function(err, res) {
        if (err) reject(err);
        else resolve(res);
      });
    });
  }

  /**
   * 删除key中的filed
   * @param {*} key
   * @param {*} filed
   */
  delValByField(key, filed) {
    return this.client.hdel(key, filed);
  }

  /**
   * 删除key
   * @param {*} key
   */
  async delValByKey(key, callback) {
    const result = await new Promise((resolve, reject) => {
      this.client.del(key, function(err, result) {
        if (err) reject(err);
        else resolve(result);
      });
    });
    return _.isFunction(callback) ? callback(null, result) : result;
  }

  /**
   * 执行lua脚本 加上乐观锁默认过期时间30s
   * @param {*} key
   * @param {*} time=30
   */
  async lockKey(key, time=30) {
    const redisLua = RedisLua(this.client);
    const r = await redisLua.run('isVote', key, time);
    return r;
  }

  /**
  * 执行lua脚本 释放乐观锁
  * @param {*} key
  * @param {*} time=30
  */
  async unlockKey(key) {
    return this.client.del(key);
  }

  /**
   * 执行事务脚本
   * @param {*} command
   */
  async execMulit(command) {
    return new Promise((resolve, reject) => {
      this.client.multi(command).exec((err, result)=> {
        if (err) reject(err);
        else resolve(result);
      });
    });
  }

  /**
   *
   * @param {*} key
   * @param {*} start
   * @param {*} end
   * @param {*} m
   */
  async zrange(key, start, end) {
    return new Promise((resolve, reject) => {
      this.client.zrange(key, start, end, 'WITHSCORES', function(err, result) {
        if (err) reject(err);
        else {
          resolve(result);
        }
      });
    });
  }
};
