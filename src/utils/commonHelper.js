'use strict';
const async = require('async');
const _ = require('lodash');

exports.promiseAllByReturn = async (tasks, num) => {
  if (!_.isArray(tasks)) return Promise.reject('arguments tasks must be array');
  let len = num;
  if (!_.isNumber(len)) {
    len = tasks.length;
  }
  return new Promise((resolve, reject) => {
    async.parallelLimit(tasks, len, function(err, results) {
      if (err) reject(err);
      else resolve(results);
    });
  });
};

exports.promiseAll = async (coll, task, num) => {
  if (!_.isArray(coll)) return Promise.reject('arguments coll be array');
  if (!_.isFunction(task)) return Promise.reject('arguments task must be funtion');
  let len = num;
  if (!_.isNumber(len)) {
    len = coll.length;
  }
  return new Promise((resolve, reject) => {
    async.eachLimit(coll, len, async function(item) {
      await task(item);
    }, function(err) {
      if (err) reject(err);
      else resolve();
    });
  });
};

