'use strict';
const jwt = require('jsonwebtoken');
/**
 *产生token帮助类
 */
class Token {
  /**
   *
   * @param {*} signature
   */
  constructor(signature) {
    this.signature = signature;
  }

  /**
   *生成token
   * @param {*} data
   */
  createToken(data) {
    return jwt.sign(data, this.signature);
  }

  /**
   *解析token
   * @param {*} token
   */
  verifyToken(token) {
    if (!token || typeof token !== 'string') return false;
    const decoded = jwt.verify(token, this.signature);
    return decoded;
  }
}

module.exports = function(signature) {
  return new Token(signature);
};
