'use strict';
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
module.exports = class MongoDb {
  /**
   *
   * @param {*} options
   */
  constructor(options) {
    this.url = options.host || 'mongodb://127.0.0.1:';
    this.port = options.port || 27017;
    this.name = options.name;
    this._url = this.url+this.port+'/'+this.name;
  }
  /**
   *
   */
  async connect() {
    return mongoose.connect(this._url, {useNewUrlParser: true});
  }
};
