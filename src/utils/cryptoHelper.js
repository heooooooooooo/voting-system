'use strict';
const uuid = require('uuid');
const crypto = require('crypto');

// 提供加密算法
module.exports = function() {
  const cj = {};

  cj.UUID = function() {
    return uuid.v1().replace(/[-]/g, '');
  };
  cj.MD5 = function(text) {
    let _text = text;
    return crypto.createHash('md5').update(_text +='').digest('hex');
  };

  cj.SHA1 = function(text) {
    let _text = text;
    return crypto.createHash('sha1').update(_text +='').digest('hex');
  };

  cj.SHA256 = function(text) {
    let _text = text;
    return crypto.createHash('sha1').update(_text +='').digest('hex');
  };
  return cj;
}();
