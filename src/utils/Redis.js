'use strict';
const redis = require('redis');
module.exports = class RedisConnect {
  /**
   *
   * @param {*} options
   */
  constructor(options) {
    this.url = options.host;
    this.option = options.option || {};
    this.client = null;
    this.connect();
  }
  /**
   *
   */
  connect() {
    this.client = redis.createClient(this.url, this.option);
  }
};
