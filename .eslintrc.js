module.exports = {
    "extends": "google",
    "parserOptions": {
        "ecmaVersion": 6
    },
    "parser": "babel-eslint",
    "rules": {
        "camelcase": "off",
        "new-cap": [1, { "capIsNew": false }],
        "valid-jsdoc": "off",
        "max-len": "off",
        'linebreak-style': 'off',
        "no-param-reassign": ["error", { "props": false }]
    },
    "globals": {
        "app": true
    }
};