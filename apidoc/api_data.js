define({ "api": [
  {
    "type": "post",
    "url": "/admin/login",
    "title": "管理员登陆获取token",
    "name": "login",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>候选人的名字.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pwd",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "header": {
      "examples": [
        {
          "title": "Header-Example:",
          "content": "{\n  \"Authorization\": \"Basic dXNlcm5hbWU6cGFzc3dvcmQ=\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"username\": \"username\",\n        \"email\": \"zheng@qq.com\",\n        \"_id\": \"5c4200140260f783d57bdb17\",\n        \"accessToken\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXJuYW1lIiwiaWF0IjoxNTQ3ODMwOTc1fQ.T9OSsEuzHTi_8OZMrYbZdOkWLGkRyLeUXSud-1tAD_c\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4012,\n    \"msg\": \"Username or password does not exist\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "admin"
  },
  {
    "type": "post",
    "url": "/candidate/add",
    "title": "增加候选人",
    "name": "add",
    "group": "candidate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>候选人的名字.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cid",
            "description": "<p>选举配置id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{ success: true,\n  data:\n   {\n     _id: '5c41ce459142591eec9dc1e2',\n     username: '郑崇',,\n     cid: '5c41cf276171242364f65773',\n     created_time: '2019-01-18T13:01:57.264Z',\n     update_time: '2019-01-18T13:01:57.264Z',\n     vote_count: 0\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4012,\n    \"msg\": \"Missing required parameters\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "candidate"
  },
  {
    "type": "delete",
    "url": "/candidate/:id",
    "title": "删除候选人",
    "name": "del",
    "group": "candidate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>候选人的id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{ success: true,\n  data:\n   {\n     id: '5c41ce459142591eec9dc1e2'\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4012,\n    \"msg\": \"Missing required parameters\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "candidate"
  },
  {
    "type": "get",
    "url": "/candidate/list/:id",
    "title": "候选人列表",
    "name": "list",
    "group": "candidate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>候选人的id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"_id\": \"5c41eb7531ec3b26080c8569\",\n            \"cid\": \"5c41cf276171242364f65773\",\n            \"username\": \"郑崇\",\n            \"vote_count\": 0,\n            \"created_time\": \"2019-01-18T15:06:29.776Z\",\n            \"update_time\": \"2019-01-18T15:06:29.776Z\",\n            \"__v\": 0\n        },\n        ...\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4011,\n    \"msg\": \"Missing required parameters\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "candidate"
  },
  {
    "type": "post",
    "url": "/candidate/update",
    "title": "更新候选人",
    "name": "update",
    "group": "candidate",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>候选人的id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>候选人的名字.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cid",
            "description": "<p>选举配置id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{ success: true,\n  data:\n   {\n     _id: '5c41ce459142591eec9dc1e2',\n     username: '郑崇',,\n     cid: '5c41cf276171242364f65773',\n     created_time: '2019-01-18T13:01:57.264Z',\n     update_time: '2019-01-18T13:01:57.264Z',\n     vote_count: 0\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4011,\n    \"msg\": \"Missing required parameters\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "candidate"
  },
  {
    "type": "post",
    "url": "/conf/addcandidate",
    "title": "增加选举配置",
    "name": "addcandidate",
    "group": "conf",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>选举的名字.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>描述.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "startTs",
            "description": "<p>开始时间（毫秒）.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "endTs",
            "description": "<p>结束时间（毫秒）.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{ success: true,\n  data:\n   {\n     _id: '5c41ce459142591eec9dc1e2',\n     name: '美国大选',\n     desc: '2019年美国总统选举',\n     startTs: '2019-01-18T12:41:36.000Z',\n     endTs: '2019-01-28T12:41:36.000Z',\n     created_time: '2019-01-18T13:01:57.264Z',\n     update_time: '2019-01-18T13:01:57.264Z',\n     __v: 0\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4010,\n    \"msg\": \"Name required param\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/candidate.js",
    "groupTitle": "conf"
  },
  {
    "type": "post",
    "url": "/visitor/check",
    "title": "检测邮箱是否可用",
    "name": "checkemail",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>用户邮箱.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"email\": true\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\":4003,\n    \"msg\":\"The email has been registered\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "post",
    "url": "/visitor/checkvote",
    "title": "检测该用户是否还能投票",
    "name": "checkvote",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cid",
            "description": "<p>选举项的id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"cid\":\"5c41cf276171242364f65773\",\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\":true/false\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4011,// 参数不对\n    \"msg\": 'Missing required parameters'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "post",
    "url": "/visitor/getToken",
    "title": "重新获取token",
    "name": "getToken",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>用户邮箱.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"accessToken\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InpoZW5nY2hvbmc0NzBAZ21haWwuY29tIiwiaWF0IjoxNTQ3NzQxODMzfQ.WdgIGvE-O7yUFac-VMLk5Hdtd0QDINcz5lN7ePH0MO8\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4002,\n    \"msg\": \"Email required param\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4009,\n    \"msg\": \"The email has been not registered\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "get",
    "url": "/visitor/getconf",
    "title": "获取可投票的配置",
    "name": "getconf",
    "group": "visitor",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        {\n            \"_id\": \"5c41cf276171242364f65773\",\n            \"name\": \"美国大选1\",\n            \"desc\": \"2019年美国总统选举\",\n            \"startTs\": \"2019-01-18T12:41:36.000Z\",\n            \"endTs\": \"2019-01-28T12:41:36.000Z\",\n            \"created_time\": \"2019-01-18T13:05:43.526Z\",\n            \"update_time\": \"2019-01-18T13:05:43.526Z\",\n            \"__v\": 0\n        },\n        ...\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "get",
    "url": "/visitor/list/:id",
    "title": "获取获选人及可投票数量",
    "name": "list",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>选举项的id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"list\": [ // 列表\n            {\n                \"_id\": \"5c41eb7531ec3b26080c8569\",\n                \"cid\": \"5c41cf276171242364f65773\",\n                \"username\": \"郑崇1\",\n                \"vote_count\": 0\n            },\n            {\n                \"_id\": \"5c41eb7c11494223fc00548c\",\n                \"cid\": \"5c41cf276171242364f65773\",\n                \"username\": \"郑崇2\",\n                \"vote_count\": 0\n            },\n            {\n                \"_id\": \"5c41fa73c7b8a428c4cc28d2\",\n                \"cid\": \"5c41cf276171242364f65773\",\n                \"username\": \"郑崇2\",\n                \"vote_count\": 0\n            }\n        ],\n        \"count\": 2 // 可投票的数据\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4002,\n    \"msg\": \"Email required param\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4011,\n    \"msg\": 'Missing required parameters'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "post",
    "url": "/visitor/register",
    "title": "发送验证邮件",
    "name": "register",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>用户邮箱.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": {\n        \"msg\": \"The email was sent successfully. Please login to verify the email.\",\n        \"email\": \"zhengchong470@gmail.com\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4003,\n    \"msg\": \"Do not send mail repeatedly\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4006,\n    \"msg\": \"The mail format is wrong\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "post",
    "url": "/visitor/submit",
    "title": "提交投票",
    "name": "submit",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>候选人的id 多个用,隔开.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cid",
            "description": "<p>选举项的id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"cid\":\"5c41cf276171242364f65773\",\n  \"id\":\"5c41eb7531ec3b26080c8569,5c41fa73c7b8a428c4cc28d2,5c41fa73c7b8a428c4cc28d2\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": [\n        \"5c41eb7531ec3b26080c8569\",\n        \"5c41fa73c7b8a428c4cc28d2\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4013, // 投票的人数超过可投的人数\n    \"msg\": \"The params format is incorrect.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4014, // 重复投票\n    \"msg\": 'Don\\'t repeat voting'\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "  HTTP/1.1 404 Not Found\n{\n    \"success\": false,\n    \"code\": 4011,// 参数不对\n    \"msg\": 'Missing required parameters'\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  },
  {
    "type": "get",
    "url": "/visitor/verification/:token",
    "title": "验证邮件连接",
    "name": "verification",
    "group": "visitor",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>用户token.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "   HTTP/1.1 404 Not Found\n{\n   \"success\": false,\n   \"code\": 4003,\n   \"msg\": \"The email has been registered\"\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/controllers/visitor.js",
    "groupTitle": "visitor"
  }
] });
