###　 项目设计
#####  数据表设计

######  1. 管理员 (admin)
######  2. 候选人 (candidater)
######  3. 投票者 (voter)
```
/****
* 管理员 (admin)
***/
{
    _id:ObjectId,
    username:String,
    email:String,
    pwd:String,
    auth:Number,//权限
    created_time:Date, 
    update_time:Date,
} 

/****
* 投票者 (voter)
***/
{
    _id:ObjectId,
    email:String,
    registeredIp:String, // 注册ip
    status:Number,// 默认0  0.没有验证 1.已验证  
    created_time:Date,
    update_time:Date,
}

/****
* 候选人 (p.candidater)
***/
{
    _id:ObjectId,
    username:String,
    vote_count:Number, // 获得投票数量 
    cid:ObjectId, // 关联 c.candidate表的_id
    created_time:Date,
    update_time:Date
}

/****
* 选举配置(c.candidate)
****
{
    _id:ObjectId,
    name:String,
    desc:String,
    startTs:Date,
    endTs:Date,
    created_time:Date
    update_time:Date
}
```

##### 需求接口设计
```
1.检测邮件格式及是否被注册
2.发送邮件 有效期2小时 
    2.a:返回url
    2.b:返回验证code  
3.获取可投票数量
    3.a:total
    3.b:count // total人数少10人时默认2 小于等于2时 count=total 
4.提交投票结果 先将结果在redis上落地，推送到mq 排队落地到数据库
5.投票开始时间及结束时间
    5.a 前端获取结果
    5.b 提交投票接口 中间件检测
6.投票时检测是否是有效邮箱及是否重复投票  在redis上用 c_candidateId:email做key值 然后上锁
7.设置开始及结束时间
8.统计票数
9.增删改查候选人
10.中间件 同个ip 投票次数、请求频率 
11.管理员增删改查
```
##### 文件结构
```
├── apidoc       // api文档
├── apidoc.json  // api文档配置
├── app.js       // 应用启动文件 
├── conf          //配置文件
│   ├── conf.default.js    // 基础配置
│   ├── conf.prod.js       // 生产配置
│   ├── conf.test.js       // 测试配置
│   └── index.js   
├── logs                   // 日志
├── node_modules
├── package-lock.json
├── package.json
├── public
│   └── 01.jpg
├── README.md
├── src     // 源码
│   ├── common  // 通用类
│   │   ├── CacheServer.js  
│   │   └── redisLua.js
│   ├── configs  // 通用配置 
│   │   ├── error.js  // 错误码对应语言描述
│   │   ├── keys.js   // redis key统一管理配置
│   │   └── logs.js   // 日志配置 
│   ├── controllers   // 控制器
│   │   ├── candidate.js
│   │   ├── error.js
│   │   ├── index.js
│   │   └── visitor.js
│   ├── middleware   // 中间件
│   │   └── index.js
│   ├── models    // 数据库model配置 
│   │   ├── admin.js
│   │   ├── c.candidate.js
│   │   ├── index.js
│   │   ├── p.candidater.js
│   │   └── voter.js
│   ├── proxys    // 数据库查询代理层
│   │   ├── admin.js
│   │   ├── Base.js
│   │   ├── c.candidate.js
│   │   ├── index.js
│   │   ├── p.candidater.js
│   │   └── voter.js
│   ├── rabbitmq  // mq服务管理
│   │   ├── index.js
│   │   ├── public.js
│   │   ├── server.js
│   │   └── subscribe.js
│   ├── router  // 路由
│   │   └── index.js
│   ├── servers     // 服务类
│   │   ├── Auth.js
│   │   └── Email.js
│   └── utils    // 通用工具类及方法
│       ├── commonHelper.js
│       ├── cryptoHelper.js
│       ├── logger.js
│       ├── Mongodb.js
│       ├── Rabbitmq.js
│       ├── Redis.js
│       ├── redisHelper.js
│       └── tokenHelper.js
└── test  // 测试文件
    ├── admin.test.js
    ├── redisHelper.test.js
    ├── support.js
    └── visitor.test.js
```
##### 使用的服务
- mongodb 持久化存储
- redis 缓存
- rabbitmq mq队列任务
- pm2 启动文件及守护进程 
 
### 开发环境

```
// vscode F5进入调试模式
// .vscode/launch.json 配置
{ 
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "restart": true,
            "runtimeExecutable": "nodemon",
            "name": "启动程序",
            "console": "integratedTerminal",
            "program": "${workspaceFolder}\\app.js",
            "internalConsoleOptions": "neverOpen"
        }
    ]
}
```

##### 生成文档
```
npm i apidoc -g 
npm run apidoc
```
然后打开 http://localhost:30001/
![image](https://gitlab.com/heooooooooooo/voting-system/raw/master/public/01.jpg)


### 测试环境
```
npm i apidoc -g 
npm i eslint -g 
npm run test:build 
```
- 启动eslient 代码检测
- 生成测试文档
- 测试用例
- 然后启动测试环境
- 

### 生产环境
```
npm run prod 
```
- pm2 启动