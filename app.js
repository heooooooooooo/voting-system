'use strict';
const Koa = require('koa');
const server = new Koa();
const bodyParser = require('koa-bodyparser');
const conf = require('./conf');
const router = require('./src/router');
const serve = require('koa-static');
const moment = require('moment');
const _ = require('lodash');
const RedisHelper = require('./src/utils/redisHelper');
const Mongodb = require('./src/utils/Mongodb');
const okeys = require('./src/configs/keys');
const middleware = require('./src/middleware');
const logUtil = require('./src/utils/logger');
const Rabbitmq = require('./src/utils/Rabbitmq');
const TokenHelper = require('./src/utils/tokenHelper');
const cryptoHelper = require('./src/utils/cryptoHelper');
const rabbitmq = require('./src/rabbitmq');
const proxys = require('./src/proxys');
const limit = require('koa-limit');
const helmet = require('koa-helmet');
const wrapper = require('co-redis');
/**
 *全局工具管理
 */
global.app = {
  tool: {},
};
app.tool.moment = moment;
app.tool._ = _;
app.conf = conf;
app.okeys = okeys;
app.logUtil = logUtil;
app.cryptoHelper = cryptoHelper;
app.proxys = proxys;
app.tokenHelper = new TokenHelper(conf.tokenSignature);
app.redisClient = new RedisHelper(conf.redis);

// mongodb连接
const db = new Mongodb(conf.mongodb);
db.connect().then((client) =>{
  app.db = client;
});

// mq连接
const mq = new Rabbitmq(conf.rabbitmq);
mq.createChannel().then(async (ch)=>{
  app.ch = ch;
  app.public = await rabbitmq.public(app.ch);
  app.subscribe = await rabbitmq.subscribe(app.ch);
});
// 断开重连
mq.on('success', async function() {
  app.ch = null;
  app.public =null;
  app.subscribe = null;
  app.ch = await mq.createChannel();
  app.public = await rabbitmq.public(app.ch);
  app.subscribe = await rabbitmq.subscribe(app.ch);
});

// 按顺序加载中间件
server.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', ctx.headers.origin);
  ctx.set('Access-Control-Allow-Headers', 'content-type');
  ctx.set('Access-Control-Allow-Methods', 'OPTIONS,GET,HEAD,PUT,POST,DELETE,PATCH');
  await next();
});

// 5min内限制用户访问不超过200次持久化到redis
server.use(limit({
  limit: 200,
  interval: 1000 * 60 * 5,
  store: wrapper(app.redisClient.client),
}));
server.use(helmet());
if (process.env.NODE_ENV !== 'production') {
  server.use(serve(__dirname + '/apidoc'));
}
server.use(serve( __dirname + '/public'));
server.use(bodyParser());
server.use(middleware.response);

server.use(router.routes());
server.use(router.allowedMethods());

server.on('error', (err, ctx) => {
  console.error('server error', err, ctx);
});

process.on('unhandledRejection', (reason, p) => {
  console.log('未处理的 rejection：', p, '原因：', reason);
});

module.exports = server.listen(conf.port || 30001, '0.0.0.0');
