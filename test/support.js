
const conf = require('./../conf');
const RedisHelp = require('./../src/utils/redisHelper');
redisClient = new RedisHelp(conf.redis);
// 测试管理员账号密码
// const user = {
//   '_id': ObjectId('5c4200140260f783d57bdb17'),
//   'username': 'username',
//   'email': 'zheng@qq.com',
//   'pwd': '2e9e3c09a29e2e6a7d1d3806fac9f02be42b58e2', // password
//   'auth': 100,
// };
// 登陆生成的请求头
// const basicAuth = {'key': 'Authorization', 'value': 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=', 'description': ''};
/**
 * 获取管理员token
 */
exports.getAdminToken = async function(request) {
  return new Promise((resolve, reject) => {
    request.post('/candidate/login')
        .set('Authorization', 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=')
        .expect(200, (err, res) => {
          if (err) reject(err);
          else resolve(res.body.data);
        }); ;
  });
};


/**
 * 获取投票者token
 */
exports.getVoterToken = async function(request) {
  return new Promise((resolve, reject) => {
    request.post('/candidate/login')
        .set('Authorization', 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=')
        .expect(200, (err, res) => {
          if (err) reject(err);
          else resolve(res.body.data);
        }); ;
  });
};

/**
 * 获取发送邮件的连接
 */
exports.getEmailUrl = async function() {
  const tokenKeys = await new Promise((resolve, reject) => {
    redisClient.client.KEYS('voterAccessToken:*', function(err, tokenKeys) {
      if (err) reject(err);
      else resolve(tokenKeys);
    });
  });
  const token = tokenKeys[0].split(':');
  return '/visitor/verification/' + `${token[1]}`;
};

/**
 * 测试开始前清除数据
 */
exports.clearVoterAccessToken = async function() {
  const tokenKeys = await new Promise((resolve, reject) => {
    redisClient.client.KEYS('voterAccessToken:*', function(err, tokenKeys) {
      if (err) reject(err);
      else resolve(tokenKeys);
    });
  });
  const command=[];
  tokenKeys.forEach((item) => {
    command.push(['del', item]);
  });
  await redisClient.execMulit(command);
};

