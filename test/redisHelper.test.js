process.env.NODE_ENV == 'test';
const should = require('should');
const conf = require('./../conf');
const RedisHelp = require('./../src/utils/redisHelper');
let redisClient;
/**
 * 测试redis工具库
 */
before(function(done) {
  redisClient = new RedisHelp(conf.redis);
  done();
});

describe('RedisHelp setDataByKey', () => {
  const key = 'setDataByKey';
  it('setDataByKey for object:', (done) => {
    redisClient.setDataByKey(key, {test: 'mocha'}, function(err, result) {
      should.not.exist(err);
      (result).should.be.ok();
      done();
    });
  });
  it('getDataByKey for object:', (done) => {
    redisClient.getValByKey(key, function(err, result) {
      should.not.exist(err);
      (result).should.have.property('test', 'mocha');
      done();
    });
  });

  it('setDataByKey for boolean:', (done) => {
    redisClient.setDataByKey(key, true, function(err, result) {
      should.not.exist(err);
      (result).should.be.ok();
      done();
    });
  });

  it('getDataByKey for boolean:', (done) => {
    redisClient.getValByKey(key, function(err, result) {
      should.not.exist(err);
      (result).should.equal(true);
      done();
    });
  });

  it('setDataByKey for number:', (done) => {
    redisClient.setDataByKey(key, 1, function(err, result) {
      should.not.exist(err);
      (result).should.be.ok();
      done();
    });
  });

  it('getDataByKey for number:', (done) => {
    redisClient.getValByKey(key, function(err, result) {
      should.not.exist(err);
      (result).should.equal(1);
      done();
    });
  });

  it('setDataByKey for string:', (done) => {
    redisClient.setDataByKey(key, 'string', function(err, result) {
      should.not.exist(err);
      (result).should.be.ok();
      done();
    });
  });

  it('getDataByKey for string:', (done) => {
    redisClient.getValByKey(key, function(err, result) {
      should.not.exist(err);
      (result).should.equal('string');
      done();
    });
  });


  after((done) => {
    redisClient.delValByKey(key, (err, result) => {
      done();
    });
  });
});


describe('RedisHelp setDataByfield', () => {
  const key = 'setDataByfield';
  const field = 'field';
  it('setDataByfield for Object:', (done) => {
    redisClient.setDataByfield(key, field, {key: 'filed'}, function(err, result) {
      should.not.exist(err);
      done();
    });
  });

  it('getDataByfield for Object:', (done) => {
    redisClient.getDataByfield(key, field, function(err, result) {
      should.not.exist(err);
      (result).should.have.property('key', 'filed');
      done();
    });
  });

  it('setDataByfield for Number:', (done) => {
    redisClient.setDataByfield(key, field, 122, function(err, result) {
      should.not.exist(err);
      done();
    });
  });

  it('getDataByfield for Number:', (done) => {
    redisClient.getDataByfield(key, field, function(err, result) {
      should.not.exist(err);
      (result).should.equal(122);
      done();
    });
  });

  it('setDataByfield for String:', (done) => {
    redisClient.setDataByfield(key, field, 'String', function(err, result) {
      should.not.exist(err);
      done();
    });
  });

  it('getDataByfield for String:', (done) => {
    redisClient.getDataByfield(key, field, function(err, result) {
      should.not.exist(err);
      (result).should.equal('String');
      done();
    });
  });

  it('setDataByfield for Boolean:', (done) => {
    redisClient.setDataByfield(key, field, 'Boolean', function(err, result) {
      should.not.exist(err);
      done();
    });
  });

  it('getDataByfield for Boolean:', (done) => {
    redisClient.getDataByfield(key, field, function(err, result) {
      should.not.exist(err);
      (result).should.equal('Boolean');
      done();
    });
  });
  after((done) => {
    redisClient.delValByKey(key, (err, result) => {
      done();
    });
  });
});
