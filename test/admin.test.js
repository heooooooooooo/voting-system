process.env.NODE_ENV == 'test';
const server = require('../app');
const request = require('supertest')(server);
const should = require('should');
const support = require('./support');
let accessToken;


describe('admin', () => {
  before(function(done) {
    support.getAdminToken(request).then(function(result) {
      accessToken = result.accessToken;
      done();
    });
  });
  let candidate = {
    desc: '2019年美国总统选举',
    startTs: 1547815296000,
    endTs: 1548679296000,
  };
  it('增加选举配置项:缺少名字', (done) => {
    candidate.accessToken = accessToken;
    request.post('/candidate/addcandidate')
        .send(candidate)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', false);
          res.body.code.should.equal(4010);
          done();
        });
  });
  it('增加选举配置项:正常添加', (done) => {
    candidate = {
      name: '美国大选',
      desc: '2019年美国总统选举',
      startTs: 1547815296000,
      endTs: 1548679296000,
      accessToken: accessToken,
    };
    request.post('/candidate/addcandidate')
        .send(candidate)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          res.body.data.name.should.equal(candidate.name);
          candidate = res.body.data;
          done();
        });
  });

  let person = {
    username: '郑崇3',
    accessToken: accessToken,
    cid: '5c41cf276171242364f65773', // 关联 c.candidate表的_id
  };
  it('增加获选人', (done) => {
    person.accessToken = accessToken;
    request.post('/candidate/add')
        .send(person)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          person = res.body.data;
          done();
        });
  });

  it('更新候选人', (done) => {
    const updatename = 'updatename';
    request.post('/candidate/update')
        .send({
          _id: person._id,
          username: updatename,
          accessToken: accessToken,
          cid: person.cid,
        })
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          res.body.data.username.should.be.equal(updatename);
          done();
        });
  });

  it('候选人列表', (done) => {
    request.get('/candidate/list/5c41eb7c11494223fc00548c?token='+accessToken)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          done();
        });
  });
  it('删除获选人', (done) => {
    request.delete('/candidate/' + person._id)
        .set('accessToken', accessToken)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          done();
        });
  });
});
after(() => {
  process.exit();
});
