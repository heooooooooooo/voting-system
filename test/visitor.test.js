process.env.NODE_ENV== 'test';
const server = require('../app');
const request = require('supertest')(server);
const should = require('should');
const support = require('./support');

const user = {
  'email': Date.now()+'_zhengchong@gmail.com',
};

before((done) => {
  support.clearVoterAccessToken().then((data) => {
    done();
  });
});

describe('visitor', ()=> {
  it('检测email是否可用:', (done)=> {
    request.post('/visitor/check')
        .send(user)
        .expect(200, (err, res)=> {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          done();
        });
  });
  it('发送验证邮件:返回成功', (done) => {
    request.post('/visitor/register')
        .send(user)
        .expect(200, (err, res) => {
          if (res.body.success) {
            should.not.exist(err);
            res.body.should.have.property('success', true);
            res.body.data.email.should.equal(user.email);
          } else {
            console.log('已经发送过一次');
            res.body.should.have.property('success', false);
            res.body.code.should.equal(4005);
          }
          done();
        });
  });
  it('一分钟内发送多次验证邮件：返回错误', (done) => {
    request.post('/visitor/register')
        .send(user)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', false);
          res.body.code.should.equal(4005);
          done();
        });
  });
  it('邮件格式不正确：返回错误', (done) => {
    request.post('/visitor/register')
        .send({email: 'test.com'})
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', false);
          res.body.code.should.equal(4006);
          done();
        });
  });

  it('未邮件确认用户获取token返回错误', (done) => {
    request.post('/visitor/getToken')
        .send(user)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', false);
          res.body.code.should.equal(4009);
          done();
        });
  });
});

// 验证连接 并且提交投票
describe('visitor vify eamil link', () => {
  let accessToken; let link;
  before((done)=> {
    support.getEmailUrl().then((data) => {
      link = data;
      done();
    });
  });

  it('验证连接', (done) => {
    request.get(link)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          done();
        });
  });

  it('用户获取token', (done) => {
    request.post('/visitor/getToken')
        .send(user)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          accessToken = res.body.data.accessToken;
          done();
        });
  });

  let voteConf;
  let candidateConfs;
  it(' 获取可投票的配置', (done) => {
    request.get('/visitor/getconf')
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          voteConf = res.body.data[0];
          done();
        });
  });

  it('获取获选人及可投票数量', (done) => {
    request.get('/visitor/list/' + voteConf._id)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          candidateConfs = res.body.data;
          done();
        });
  });

  it('提交投票', (done) => {
    const data = {
      accessToken: accessToken,
      cid: candidateConfs.list[0].cid,
      id: candidateConfs.list[0]._id,
    };
    request.post('/visitor/submit')
        .send(data)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', true);
          done();
        });
  });

  it('重复投票提交投票 返回失败', (done) => {
    const data = {
      accessToken: accessToken,
      cid: candidateConfs.list[0].cid,
      id: candidateConfs.list[0]._id,
    };
    request.post('/visitor/submit')
        .send(data)
        .expect(200, (err, res) => {
          should.not.exist(err);
          res.body.should.have.property('success', false);
          res.body.should.have.property('code', 4014);
          done();
        });
  });
});
after(() => {
  process.exit();
});
