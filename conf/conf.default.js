let conf ={};
if (process.env.NODE_ENV=='test') {
  conf = require('./conf.test');
} else if (process.env.NODE_ENV=='production') {
  conf = require('./conf.prod');
}
const defaultConf = {
  port: 30001,
  redis: { // redis配置
    host: 'redis://127.0.0.1:6379',
    option: {
      db: 10,
    },
  },
  mongodb: { // 数据库配置
    host: 'mongodb://127.0.0.1:',
    port: 27017,
    name: 'dev',
  },
  rabbitmq: { // mq配置
    protocol: 'amqp',
    hostname: 'localhost',
    port: 5672,
    username: 'z_chong',
    password: '123456',
    locale: 'en_US',
    frameMax: 0,
    heartbeat: 0,
    vhost: '/develop',
  },
  tokenSignature: '^~@$1#%(%#OP2465?}$',
  voterTokenExpire: 3600 * 2,
  adminTokenExpire: 3600*2,
  email: {
    service: 'QQ',
    port: 465,
    secureConnection: true, // use SSL
    auth: {
      user: 'xx@qq.com',
      pass: 'xx',
    },
  },
  salt: 'b]39/"l1(n,yag&u',
  url: 'http://localhost:30001/visitor/verification',
  redirectUrl: 'http://localhost:30001',

};
console.log('运行环境：', process.env.NODE_ENV);
module.exports = Object.assign(defaultConf, conf);
